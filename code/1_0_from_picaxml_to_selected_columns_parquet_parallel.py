#!/usr/bin/env python3

# -*- coding: utf-8 -*-
"""
Created on 2021

@author: jct
"""

import os
import shutil
import glob
from lxml import etree
import pandas as pd
import sys
import requests
import time

print("imports ok")

sys.path.append(os.path.abspath("./../../"))


from librarian_robot import transform_formats


print("import librarian robot also ok")


xpaths_fields = {
        
            'ppn' : './pica:datafield[@tag="003@"]/pica:subfield[@code="0"]/text()',
            'medium' : './pica:datafield[@tag="002@"]/pica:subfield[@code="0"]/text()',
            'title' : './pica:datafield[@tag="021A"]/pica:subfield[@code="a"]/text()',
            'title_supplement' : './pica:datafield[@tag="021A"]/pica:subfield[@code="d"]/text()',
            'year' : './pica:datafield[@tag="011@"]/pica:subfield[@code="a"]/text()',
            'entry_first' : './pica:datafield[@tag="001A"]/pica:subfield[@code="0"]/text()',
            'author_first_name' : './pica:datafield[@tag="028A"]/pica:subfield[@code="d" or @code="D" ]/text()', 
            'author_last_name' : './pica:datafield[@tag="028A"]/pica:subfield[@code="a" or @code="A" ]/text()',
            'author_gnd_id' : './pica:datafield[@tag="028A"]/pica:subfield[@code="9"]/text()',
            'editor_first_name' : './pica:datafield[@tag="028C"]/pica:subfield[@code="d" or @code="D" ]/text()', 
            'editor_last_name' : './pica:datafield[@tag="028C"]/pica:subfield[@code="a" or @code="A" ]/text()',
            'editor_gnd_id' : './pica:datafield[@tag="028C"]/pica:subfield[@code="9"]/text()',
            #'author_gnd_notation' : './pica:datafield[@tag="028A"]/pica:subfield[@code="8"]/text()',
            'isbn' : './pica:datafield[@tag="004A"]/pica:subfield[@code="0"]/text()',
            'ILNs' : './pica:datafield[@tag="001@"]/pica:subfield[@code="0"]/text()',
            #'content_type_ppn' : './pica:datafield[@tag="013D"]/pica:subfield[@code="9"]/text()',
            'content_type' : './pica:datafield[@tag="013D"]/pica:subfield[@code="a"]/text()',
            
            
            'publisher' : './pica:datafield[@tag="033A"]/pica:subfield[@code="n"]/text()',
            'language_text' : './pica:datafield[@tag="010@"]/pica:subfield[@code="a"]/text()',
            'language_original' : './pica:datafield[@tag="010@"]/pica:subfield[@code="c"]/text()',
            'pages' : './pica:datafield[@tag="034D"]/pica:subfield[@code="a"]/text()',
            'format' : './pica:datafield[@tag="034I"]/pica:subfield[@code="a"]/text()',
            'comment_isbn' : './pica:datafield[@tag="004A"]/pica:subfield[@code="f"]/text()',
            #'issn' : './pica:datafield[@tag="005A"]/pica:subfield[@code="0"]/text()',
            #'comment_issn' : './pica:datafield[@tag="005A"]/pica:subfield[@code="f"]/text()',
            'place_publication' : './pica:datafield[@tag="033A"]/pica:subfield[@code="p"]/text()',
            'summary' : './pica:datafield[@tag="047I"]/pica:subfield[@code="a"]/text()',
            'title_continuing_resource' : './pica:datafield[@tag="036E"]/pica:subfield[@code="a"]/text()',


            'work_ppn' : './pica:datafield[@tag="022A"]/pica:subfield[@code="9"]/text()',
            'work_info' : './pica:datafield[@tag="022A"]/pica:subfield[@code="8"]/text()',
            'work_title' : './pica:datafield[@tag="022A"]/pica:subfield[@code="a"]/text()',

            'expression_ppn' : './pica:datafield[@tag="039M"]/pica:subfield[@code="9"]/text()',
            'expression_info' : './pica:datafield[@tag="039M"]/pica:subfield[@code="8"]/text()',
            'expression_title' : './pica:datafield[@tag="039M"]/pica:subfield[@code="a"]/text()',
            
            'DDC_notation' : './pica:datafield[@tag="045F"]/pica:subfield[@code="a"]/text()',
            'DDC_sachgruppe' : './pica:datafield[@tag="045G"]/pica:subfield[@code="a"]/text()',
            'DDC_grundnotation' : './pica:datafield[@tag="045H"]/pica:subfield[@code="c"]/text()',

            'BK_ppn' : './pica:datafield[@tag="045Q"]/pica:subfield[@code="9"]/text()',
            'BK_notation' : './pica:datafield[@tag="045Q"]/pica:subfield[@code="a"]/text()',
            'BK_j' : './pica:datafield[@tag="045Q"]/pica:subfield[@code="j"]/text()',

            'RVK_ppn' : './pica:datafield[@tag="045R"]/pica:subfield[@code="9"]/text()',
            'RVK_notation' : './pica:datafield[@tag="045R"]/pica:subfield[@code="a"]/text()',
            'RVK_j' : './pica:datafield[@tag="045R"]/pica:subfield[@code="j"]/text()',
            'RVK_k' : './pica:datafield[@tag="045R"]/pica:subfield[@code="k"]/text()',

            'keyword_RSWK' : './pica:datafield[@tag="041A"]/pica:subfield[@code="a" or @code="A"]/text()',
            'keyword_K10plus' : './pica:datafield[@tag="044K"]/pica:subfield[@code="a" or @code="A"]/text()',
            'keyword_project' : './pica:datafield[@tag="041L"]/pica:subfield[@code="a" or @code="A"]/text()',
            'keyword_local' : './pica:datafield[@tag="044Z"]/pica:subfield[@code="a" or @code="A"]/text()',
            'keyword_045D' : './pica:datafield[@tag="045D"]/pica:subfield[@code="a" or @code="A"]/text()',
            'keyword_LoC' : './pica:datafield[@tag="044A"]/pica:subfield[@code="a" or @code="A"]/text()',

            'lcc_notation' : './pica:datafield[@tag="045A"]/pica:subfield[@code="a"]/text()',
            'klassifikationssystem_system' :  './pica:datafield[@tag="045X"]/pica:subfield[@code="i"]/text()',
            'klassifikationssystem_notation' : './pica:datafield[@tag="045X"]/pica:subfield[@code="a"]/text()', 

            'uri_description' : './pica:datafield[@tag="209R"]/pica:subfield[@code="y"]/text()',
            'uri' : './pica:datafield[@tag="209R"]/pica:subfield[@code="a"]/text()',

            'GOK_ppn' : './pica:datafield[@tag="145Z"][./pica:subfield[@code="V"]/text()="Tev"]/pica:subfield[@code="9"]/text()',
            'GOK_notation' : './pica:datafield[@tag="145Z"][./pica:subfield[@code="V"]/text()="Tev"]/pica:subfield[@code="a"]/text()',
            'GOK_j' : './pica:datafield[@tag="145Z"][./pica:subfield[@code="V"]/text()="Tev"]/pica:subfield[@code="j"]/text()',

            'signatur_place' : './pica:datafield[@tag="209A"]/pica:subfield[@code="f"]/text()',
            'signatur' : './pica:datafield[@tag="209A"]/pica:subfield[@code="a"]/text()',
            'signatur_date' : './pica:datafield[@tag="208@"]/pica:subfield[@code="a"]/text()',

            'Abrufzeichen' : './pica:datafield[@tag="209O"]/pica:subfield[@code="a"]/text()',
            }





start_time = time.time()

workdir = sys.argv[1]
year = sys.argv[2]

transform_formats.from_picaxml_with_multiple_entries_to_parquet(
wdir = workdir + "/pica_jah_" + year + "/" ,
xpaths_fields = xpaths_fields,
ending = ".picaxml",
outdir = workdir + "/parquets/",
)
print("done", __file__)